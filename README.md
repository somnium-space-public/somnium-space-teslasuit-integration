# Somnium Space Teslasuit Integration

## Introduction
Welcome to Somnium Space Teslasuit integration guide. This guide will take you through the process of setting up everything you need to start feeling sensations in Somnium Space while wearing Teslasuit.

### Preparations
1. Download & install the latest version of Teslasuit software from Teslasuit's [developer portal](https://developer.teslasuit.io/)
2. After installing **make sure your Teslasuit is properly calibrated**. If you don’t know how to calibrate the suit, follow Teslasuit [pairing and calibration guide](https://developer.teslasuit.io/blog/suit-tutorial-pairing-calibration/). **Failing the calibration might result in not optimal experience.**
3. If your suit fails to connect via Control Center, follow Teslasuit [migration guide](https://developer.teslasuit.io/) to update your suit’s firmware
4. Once you have your suit calibrated, launch Somnium in VR mode. Once you load in the world, Somnium should automatically detect the installation of Teslasuit’s API and generate the folder structure.
Folders will be created in your **user’s Documents** folder - Usually __*C:/Users/yourUserName/Documents/Somnium Space/Teslasuit/HapticAnimations*__. 
In case of folders not being created, verify the installation of Teslasuit software.
You can now restart Somnium.

### Folder Structure
The folder structure might be confusing at first, but is very simple to use. Example of how it looks below (diagram of the same folder structure found on screenshots below):
- .../Somnium Space
    - /Teslasuit
        - /TeslasuitSettings.json
        - /HapticAnimations
            - /CreatorName1
                - /hapticAsset1.ts_asset
                - /hapticAsset2.ts_asset
            - /CreatorName2
                - /hapticAssetOfCreator2.ts_asset


Created folder structure will come empty, except settings file, which can be edited in your favorite text editor. If you don’t possess skills to modify JSON files worry not, you will be able to change your settings from Somnium tablet later.
Settings file is located in …/Documents/Somnium Space/Teslasuit folder

![](https://i.imgur.com/3baYiK2.png)

Sensation animations have to be manually moved/copied to HapticAnimations folder to be visible from the tablet. They also have to be within another folder that holds the name of the creator (see picture below).

![](https://i.imgur.com/7U4xmol.png)

![](https://i.imgur.com/6XFoU2L.png)

Once you populate your folders with .ts_asset files and the folder structure looks similar to the one on pictures above, you are ready for the next steps.

### Connecting Teslasuit in Somnium Space
1. Launch Somnium Space in VR mode as you’re used to.
2. After being loaded, open the Teslasuit app in the tablet.
3. On the top left of the tablet screen, you should see the suit is disconnected. Now is the time to connect the suit through Teslasuit’s **Control Center**.
4. Once the suit is connected, tablet should update the message saying that Teslasuit is connected.
5. You can choose from sensations previously added to the folder structure and apply them into the preset. This will save your settings for future sessions as well.
6. Happy sensation hunting.

#### Currently supported sensation triggers
 - Weather (rain & snow)
 - Flying
 - Teleport
 - Massage
 - Weapon recoil

 ## Working with sensations in WORLDs
1. Make sure your Unity project with Somnium SDK uses following tag list. These tags will be later used to trigger specific sensations. If you need to remove some tags in order to match the list below, you will need to restart Unity editor after removing tags.

![](https://i.imgur.com/izLYDrL.png)

2. Create a trigger zone using any collider, be sure to mark that collider as trigger and select appropriate tag to the haptic sensation you want to trigger.
3. Make sure the trigger you just created is in layer 29:Trigger.

![](https://i.imgur.com/4lw3YvS.png)

4. Customize using your creativity. They will work the same way all the other triggers do.
5. Keep in mind by using these triggers, you will always play sensations users have set up in their tablet. If you want to play custom sensations, you first need to share them with visitors of your world and they need to select them in the Teslasuit tablet application.



## Changelog
### Version 1.1
 - Added ability for WORLDs creators to trigger teslasuit sensations
 - Teslasuit Manager is now persistent through WORLDs transitions
 - Added changelog

### Version 1.0
 - Added Teslasuit SDK
 - Added support for 6 basic haptic sensations